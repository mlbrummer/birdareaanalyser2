/*
 * Copyright (c) 2016 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.nomi.bird_area_analyser;

/**
 *
 * @author michiel
 */
public final class BirdAreaAnalyser {
    /**
     * private constructor for utility class.
     */
    private BirdAreaAnalyser() { }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        BirdAreaAnalyser analyser = new BirdAreaAnalyser();
        analyser.start();
    }

    /**
     * starts the application controller.
     */
    private void start() {
        //When in production, get input file name through CL
        String dataFileName = "./data/britain_species.txt";
        BirdAreaSource dataSource = new FileBirdAreaSource(dataFileName);
        BirdAreaAnalysisController controller = new BirdAreaAnalysisController();
        controller.start(dataSource);
    }
}
