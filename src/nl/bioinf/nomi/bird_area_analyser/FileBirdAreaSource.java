/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.nomi.bird_area_analyser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author michiel
 */
public class FileBirdAreaSource implements BirdAreaSource {

    private final String dataFile;

    /**
     * constructs with data file.
     * @param dataFilePath
     */
    public FileBirdAreaSource(final String dataFilePath) {
        this.dataFile = dataFilePath;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    @Override
    public BirdAreaCollection getBirdAreaCollection() throws IOException {
        Path path = Paths.get(dataFile);
        if (! Files.exists(path) || ! Files.isReadable(path)) {
            throw new IOException("file does not exist or cannot be read: " + dataFile);
        }
        BirdAreaCollection coll = new BirdAreaCollection();
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset())) {
            //island;area;elevation;soil.types;latitude;dis.from.britain;num-species
            //line = Ailsa;0.8;340;1;55.3;14.0;75
            String line;
            int lineCount = 0;
            while ((line = reader.readLine()) != null) {
                lineCount++;
                //System.out.println("lineCount = " + lineCount);
                //System.out.println("line = " + line);
                if (lineCount == 1) {
                    continue;
                }
                String[] elements = line.split(";");
                String island = elements[0];
                Double area = Double.parseDouble(elements[1]);
                int elevation = Integer.parseInt(elements[2]);
                short soilTypes = Short.parseShort(elements[3]);
                double latitude = Double.parseDouble(elements[4]);
                double distFromBritain = Double.parseDouble(elements[5]);
                int speciesNumber = Integer.parseInt(elements[6]);
                Area a = new Area(island, area, elevation, soilTypes, latitude, distFromBritain, speciesNumber);
                coll.addArea(a);
            }
        } catch (IOException ex) {
            throw new IOException("error while parsing file", ex);
        }
        return coll;
    }
}
